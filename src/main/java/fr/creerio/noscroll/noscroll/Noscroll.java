/**
 * NoScroll - Disable hotbar scrolling
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package fr.creerio.noscroll.noscroll;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import org.lwjgl.glfw.GLFW;

@Environment(EnvType.CLIENT)
public class Noscroll implements ClientModInitializer {

    /**
     * Boolean defining if the mod effects are enabled, or not
     * Enabled by default, unless the user decides to disable it
     */
    private static boolean isEnabled = true;

    @Override
    public void onInitializeClient() {
        // Register key
        KeyBinding toggleKey = KeyBindingHelper.registerKeyBinding(
            new KeyBinding(
                "key.noscroll.toggle",
                InputUtil.Type.KEYSYM,
                GLFW.GLFW_KEY_H,
                "category.noscroll"
            )
        );

        // Action to execute when key pressed
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            while (toggleKey.wasPressed()) {

                // User must in a world
                if (client.player != null) {
                    Text actionBarMsg;
                    if (isEnabled)
                        actionBarMsg = new TranslatableText("msg.disableNoScroll");
                    else
                        actionBarMsg = new TranslatableText("msg.enableNoScroll");

                    // Set enabled, or disabled depending on the situation
                    updateModEnabled(!isEnabled);

                    // Hotbar message for the player
                    client.player.sendMessage(actionBarMsg, true);

                    // Plays a sound to the player, indicating a change of state
                    assert client.world != null;
                    client.world.playSound(
                            client.player,
                            client.player.getBlockPos(),
                            SoundEvents.BLOCK_ANVIL_LAND,
                            SoundCategory.PLAYERS,
                            0.15f,
                            2.0f
                    );
                }

            }
        });
    }

    /**
     * Allows the enabled variable to be updated
     *
     * @param enabled
     * If the scrolling should be enabled or not
     */
    private static void updateModEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    /**
     * Returns if the mod effects are enabled, or not
     * @return true or false
     */
    public static boolean isEnabled() {
        return isEnabled;
    }
}
