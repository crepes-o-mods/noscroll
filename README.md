# NoScroll

This mod disables hotbar scrolling. A keybind makes it possible to enable it again, should you want to.

It is intended for personal use, but if you want to use it, go ahead.

## FAQ

* Version support ?

> See the current branch name.
> For more context : let's say there is a 1.16.2+ and a 1.19+ branch, it means that the 1.16.2+ branch should work with minecraft versions between 1.16.2 and 1.18.2.

* Any other ?

> Technically, this mod is 1.14+ compatible because of the way it works. However, you cannot use it with versions under 1.16.2 since the keybinding system given by fabric works a bit differently. If you remove that, you should be able to use it from 1.14 to 1.19.4+, without a way to modify if the scrolling is enabled or not.
> Some limitation comes from minecraft themselves : because of the "nice" messages put on the action bar, the main class of this mod has to be modified for Minecraft 1.19 to handle properly translated text.

* Dependencies ?

> You need [Fabric API](https://modrinth.com/mod/fabric-api), this is necessary to register keybinds unfortunately.

* Can I fork it ?

> Yes you can. You must however do so accordingly to the [license](LICENSE), a.k.a GPLv3. If possible, please use another name (this is not required, but it's a nice effort if you can do that).
